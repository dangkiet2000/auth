const LOGIN_SUCCESS = "Logged in successfully!";
const PASSWORDS_NOT_MATCH = "Passwords did not match!";
const USER_NOT_FOUND = "Username or password wrong!";
const USER_EXIST = "Username has existed";

export { LOGIN_SUCCESS, USER_EXIST, PASSWORDS_NOT_MATCH, USER_NOT_FOUND };
