export const API_URL = import.meta.env.VITE_API_URL;
export const ERROR_PORT_USER_NOT_FOUND = 404;
export const ERROR_PORT_USER_EXIST = 409;
