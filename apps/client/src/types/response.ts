import { AxiosError } from "axios";
import { TUser } from "./user";

type TData = {
  token?: string;
  userSignInfo?: {
    username?: string;
  };
};

export type TSuccessResponse = {
  data?: TData;
  message?: string;
};

export type TErrorResponse = {
  error?: AxiosError;
};

export type TResponse = TSuccessResponse & TErrorResponse;

export type TReponseUsers = {
  message?: string;
  data?: {
    userList: TUser[];
  };
  error?: AxiosError;
};
