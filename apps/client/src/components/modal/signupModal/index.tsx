import { Box, Typography } from "@mui/material";
import Alert from "@mui/material/Alert";

import CustomButton from "@/components/button";
import InputField from "@/components/inputField";

import { ChangeEvent, useMemo, useState } from "react";
import { toast } from "react-hot-toast";
import authApi from "@/api/modules/auth.modules";
import { PASSWORDS_NOT_MATCH, USER_EXIST } from "@/configs/notification";
import { useModalContext } from "@/providers/modalProvider";
import { ERROR_PORT_USER_EXIST } from "@/configs/appConfigs";
import { TAuth } from "@/types/auth";
import { TResponse } from "@/types/response";
import { comparison } from "@/utils/comparison";

const SignUpModal = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const { onClose } = useModalContext();

  const [formError, setFormError] = useState("");
  const [responseError, setResponseError] = useState("");

  const handleChangeUsername = (event: ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };

  const handleChangePassword = (event: ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  const handleChangeConfirmPassword = (
    event: ChangeEvent<HTMLInputElement>
  ) => {
    setConfirmPassword(event.target.value);
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    setFormError("");
    setResponseError("");

    if (!comparison(password, confirmPassword)) {
      setFormError(PASSWORDS_NOT_MATCH);
      return;
    }

    const userData: TAuth = { username, password };
    const response: TResponse = await authApi.signup(userData);

    if (response?.message) {
      toast.success(response.message);

      return onClose();
    }

    if (
      comparison(
        response.error?.response?.status as number,
        ERROR_PORT_USER_EXIST
      )
    ) {
      setResponseError(USER_EXIST);
    }
  };

  const sxButton = useMemo(() => {
    return { paddingX: 8, marginTop: 2 };
  }, []);

  return (
    <Box component={"form"} mt={2} onSubmit={handleSubmit}>
      <Box display="flex" alignItems="center">
        <Typography variant="body1" width="120px">
          Username
        </Typography>
        <InputField
          label="Username"
          value={username}
          setValue={handleChangeUsername}
        />
      </Box>

      <Box mt={2} display="flex" alignItems="center">
        <Typography variant="body1" width="120px">
          Password
        </Typography>
        <InputField
          label="Password"
          value={password}
          setValue={handleChangePassword}
          type="password"
        />
      </Box>

      <Box mt={2} display="flex" alignItems="center">
        <Typography variant="body1" width="120px">
          Confirm Password
        </Typography>
        <InputField
          label="Confirm Password"
          value={confirmPassword}
          setValue={handleChangeConfirmPassword}
          type="password"
        />
      </Box>

      {formError && (
        <Alert sx={{ marginTop: 2 }} severity="error">
          {formError}
        </Alert>
      )}

      {responseError && (
        <Alert sx={{ marginTop: 2 }} severity="error">
          {responseError}
        </Alert>
      )}

      <Box display="flex" justifyContent="center">
        <CustomButton isSubmit label="Sign up" sx={sxButton} />
      </Box>
    </Box>
  );
};

export default SignUpModal;
